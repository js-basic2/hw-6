"use strict";
/* Теоретичні питання
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

Екранування слугує для того, щоб інтерпетатор зміг розрізнити, чи знак є констуркцією кода, чи знак є текстовим включенням. Для того, щоб 
код не зламався, треба екранувати символи за допомогою \. Наприклад, 'Однажды Арнольд сказал: "I'll be back"'; буде помилка, тому що після букви І
стоїть одинарна закриваюча лапка. Але якщо вказати знак екранування: 'Однажды Арнольд сказал: "I\'ll be back"', це буде коректно.  

2. Які засоби оголошення функцій ви знаєте?

 - Function Declaration:
    function multiply(x,y) {
     return x * y;
      }
     multiply(2, 2);

- Function Expression:
     const multiply = function(x,y) {
     return x * y;
       }
multiply(2, 2);

- Cтрілкова функція:
        const multiply = (x, y) => { return x * y; };
           multiply(2, 2) );

- Об'єктний метод:

const mathLib = {
  PI: 3.14,
  multiply: function(x, y) {
    return x * y;
  },
  divide: function(x, y) {
    return x / y;
  }

}
console.log( mathLib.multiply(2, 2) )

3. Що таке hoisting, як він працює для змінних та функцій?

Hoisting - це «підняття». Це доступ до змінних або до функції до їх об'яви. Наприклад:
console.log(foo);  
var foo = "Tom";   

або 
display();
 
function display(){
    console.log("Hello Hoisting"); 
}

Як бачим, виклик змінної та функції здійснюється до їх ініціалізації. Це можливо завдяки роботі компілятора, який робить два проходи.
На першому проході компілятор отримує всі доступи до змінних, на другому проході - починає виконувати код.   
*/ 

function User() {
  this.firstName = prompt("Ваше имя?");
  this.lastName = prompt("Ваша фамилия");
  this.birth = prompt("Дата рождения");

  while(!this.firstName && !this.lastName) {
    this.firstName = prompt("Вы не ввели ваше имя?");
    this.lastName = prompt("Вы не ввели вашу фамилию");
  }

  this.getLogin = function() {
   return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
   };


  this.getAge = function() {  
      let year = +this.birth.slice(-4); 
      let month = +this.birth.slice(3,5) - 1; 
      let day = +this.birth.slice(0,3); 

      let birthday = new Date(year, month, day); // data of birth
      let now = new Date();     // Текущая дата 
      let today = new Date(now.getFullYear(), now.getMonth(), now.getDate()); // дата без минут 
      let birthdayInThisYear = new Date(today.getFullYear(), birthday.getMonth(), birthday.getDate()); // birthday in this year 
      let age = today.getFullYear() - birthday.getFullYear(); 
      if(today < birthdayInThisYear) { 
      age = age - 1;} 

   return(`Возраст: ${age}`); 

    } 


  this.getPassword = function() {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birth.slice(-4);}
};

let newUser = new User();
console.log(newUser.getLogin()); 
console.log(newUser.getPassword());
console.log(newUser.getAge());